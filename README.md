The Lazy Dog crew first and foremost values an authentic, freshly baked slice of pizza perfection. We deliver pizza, stromboli, and salads to the Frankenmuth community and surrounding areas.

Address: 154 S Main St, #5, Frankenmuth, MI 48734, USA

Phone: 989-262-4242

Website: https://lazydogpizza.com
